import time
import re
import json
import os
import requests
import logging
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import NoSuchElementException
from webdriver_manager.chrome import ChromeDriverManager



class BotDSA:
    # conda install -c conda-forge --name myenv selenium
    # docker pull selenium/standalone-firefox
    # docker run --rm -d -p 5555:4444 --shm-size=2g selenium/standalone-firefox
    def __init__(self, dados):
        super().__init__()
        path = os.getcwd()
        # self.browser = webdriver.Remote('http://localhost:5555/wd/hub', DesiredCapabilities.FIREFOX)
        # self.browser.set_window_size(1280, 1024)
        options = Options()
        options.headless = False
        #self.browser = webdriver.Firefox(options=options,executable_path=str(path)+'/geckodriver')
        self.browser = webdriver.Chrome(ChromeDriverManager().install())
        self.email = dados['email']
        self.passwd = dados['passwd']
        self.course_name = dados['course_name']
        self.init_Logger()

    def init_Logger(self):
        self.logger = logging.getLogger('botDSA')
        hdlr = logging.FileHandler('bot_dsa.log')
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        hdlr.setFormatter(formatter)
        self.logger.addHandler(hdlr)
        self.logger.setLevel(logging.INFO)

    def writeCache(self):
        file_cache = './'+self.course_name+'/cache_'+str(self.course_name).replace(' ','_')+'.txt'
        f = open(file_cache, 'w')
        # i in range(5):
        f.write(str(self.cache_cap)+'-'+str(self.cache_sec)+'\n')
        print("Write cache")
        f.close()
    
    def readCache(self):
        file_cache = './'+self.course_name+'/cache_'+str(self.course_name).replace(' ','_')+'.txt'
        self.cache_cap = 0
        self.cache_sec = 0
        if os.path.isfile(file_cache):
            with open(file_cache) as arquivo:
                for linha in arquivo:
                    mod = linha.strip().split('-')
                    self.cache_cap = int(mod[0])
                    self.cache_sec = int(mod[1])

    def baixaVideo(self, options):
        path_chapter =options['chapter']
        path_section =options['section'].replace("/","-")+".mp4"
        path_section = self.course_name+'/'+path_chapter+'/'+path_section
        if os.path.isfile(path_section):
            print("File already exists: "+path_section)
            print("** Going to the next! ** ")
            return
        r = requests.get(options['url'])
        print("****Connected****")
        if os.path.isdir('./'+self.course_name) == False:
            os.mkdir('./'+self.course_name)
            print("make dir: "+ self.course_name)
        if os.path.isdir('./'+self.course_name+'/'+path_chapter) == False:
            os.mkdir('./'+self.course_name+'/'+path_chapter)
            print("make dir: "+ path_chapter+'in the '+self.course_name)
        f = open(path_section, 'wb')
        print("Donloading.....")
        for chunk in r.iter_content(chunk_size=255):
            if chunk:  # filter out keep-alive new chunks
                f.write(chunk)
        f.close()
        self.writeCache()
        print("Done")

    def extract(self):
        self.enter_page()
        lista_cap = self.browser.find_elements_by_class_name('lrn-path-chapter')
        self.links = []
        self.chapter = ""
        self.actions = ActionChains(self.browser)
        self.conta_cap = 0
        self.cache_cap = 0
        self.readCache()
        for index in range(self.cache_cap,len(lista_cap)):
            elm = lista_cap[index]
            cap = elm.find_element_by_class_name('lrn-path-chapter-name-num')
            cap.click()
            self.find_chapter(elm)
            self.scroll_to_view(elm)
            elm.click()
            #self.actions.move_to_element(elm).perform()
            if index > self.cache_cap:
                self.cache_sec = 0
            self.conta_cap = index+1
            self.cache_cap = index
            self.find_videos()
            print("finished chapter: "+self.chapter)
        print('**** finished web scraping ****')
        #print(self.links)


    def find_videos(self):
        lista_videos = self.browser.find_elements_by_xpath('/html/body/div[4]/div/div[1]/div[3]/div/div[1]/ul/li['+str(self.conta_cap)+']/ul/li')
        self.num_section = 0
        self.num_sec_icon = self.cache_sec
        self.num_section = self.cache_sec
        
        for index in range(self.cache_sec,len(lista_videos)-1):
            try:
                video = lista_videos[index]
                video.click()
                time.sleep(5)
                self.num_section +=1
                self.cache_sec = index
                self.scroll_to_view(video)
                #self.actions.move_to_element(video).perform()

                icone = self.browser.find_elements_by_xpath('/html/body/div[4]/div/div[1]/div[3]/div/div[1]/ul/li['+str(self.conta_cap)+']/ul/li['+str(self.num_section)+']/a/img')
                ext = icone[0].get_attribute("src")[-9:-4]
                if( ext != 'video'):
                    # self.num_section -=1
                    print("It is not a vídeo! "+str(ext))
                    print("** Going to the next! **")
                    continue
                video.click()
                time.sleep(3)
                self.scroll_to_view(video)
                self.make_data_link(video)
                self.browser.switch_to.default_content()
            except Exception as e:
                self.logger.error('Error: '+ str(e))
                print(e)
                raise SystemExit

    def login(self):
        self.browser.find_element_by_link_text('ENTRAR').click()
        email = self.browser.find_element_by_class_name('-email-input')
        email.clear()
        email.send_keys(self.email)
        passwd = self.browser.find_element_by_class_name('-pass-input')
        passwd.clear()
        passwd.send_keys(self.passwd)
        self.browser.find_element_by_id('submitLogin').click()
        time.sleep(8)

    def enter_page(self):
        site = 'https://www.datascienceacademy.com.br/'
        self.browser.get(site)
        self.browser.implicitly_wait(20)
        self.login()
        # clica no perfil do usuario
        self.browser.find_element_by_id('profileLink').click()
        # clica terceiro curso da lista
        elem_curso = self.find_course(self.course_name)
        elem_curso.click()
        # clica no botao continuar
        time.sleep(8)
        # self.browser.find_element_by_xpath(
        #     '/html/body/div[6]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div').click()
        self.browser.find_element_by_xpath(
            '/html/body/div[6]/div[1]/div[2]/div[1]/div[2]/div').click()

    def find_course(self,name):
        list_courses = self.browser.find_elements_by_class_name('box-course-title')
        for course in list_courses:
            if name == course.text:
                return course
        raise NameError("Curso "+str(name)+" nao encontrado!")

    def scroll_to_view(self,element):
        x = element.location['x']
        y = element.location['y']
        scroll_by_coord = 'window.scrollTo(%s,%s);' % (x, y)
        self.browser.execute_script(
            "arguments[0].scrollIntoView();", element)

    def find_data_link(self,video):
        self.browser.switch_to.frame(
        self.browser.find_elements_by_tag_name("iframe")[1])
        try:
            self.browser.find_element_by_class_name('playerWrapper').click()
        except NoSuchElementException as e:
            self.logger.error('NoSuchElementException: '+ str(e))
            print("** Video not found!!**")
            print("** Going to the next! **")
            return []
           

        self.browser.switch_to.frame(
        self.browser.find_elements_by_tag_name("iframe")[0])
        p = re.compile(r'progressive":\[{(?s)(.*)}\]},"lang"')
        data_str = p.search(self.browser.find_elements_by_tag_name(
            "script")[-1].get_attribute('innerHTML'))

        return json.loads(data_str[0].replace(
            'progressive":', '').replace('},"lang"', ''))

    def make_data_link(self,video):
        section = video.find_element_by_class_name(
                    'lrn-path-cont-name').get_attribute('innerHTML')
        section = str(self.num_section)+' - '+section
        print("Finding video: "+section)
        data_json = self.find_data_link(video)
        for elem in data_json:
            if elem['width'] == 1920:
                url = str(elem['url'])
                data_link = {'chapter': self.chapter, 'section': section, 'url': url}
                print(data_link)
                self.baixaVideo(data_link)
                self.links.append(data_link)
                break

    def find_chapter(self,elm):
        cap_tmp = elm.find_element_by_class_name(
                'lrn-path-chapter-name-num').get_attribute('innerHTML')
        cap_tmp += elm.find_element_by_class_name(
                'lrn-path-chapter-name-txt').get_attribute('innerHTML')
        self.chapter = cap_tmp
